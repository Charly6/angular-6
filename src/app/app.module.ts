import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { ContenidoComponent } from './components/contenido/contenido.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { APP_ROUTING } from "./app.routes";
import { Opcion1Component } from './components/opcion1/opcion1.component';
import { Opcion2Component } from './components/opcion2/opcion2.component';
import { Opcion3Component } from './components/opcion3/opcion3.component';
import { Opcion4Component } from './components/opcion4/opcion4.component';
import { Opcion5Component } from './components/opcion5/opcion5.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    InicioComponent,
    ContenidoComponent,
    ContactoComponent,
    Opcion1Component,
    Opcion2Component,
    Opcion3Component,
    Opcion4Component,
    Opcion5Component
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
